# HASH STATION

Hash Station, Gitlab CI/CD pipeline çalışmaları için oluşturulmuş, [Crack Station](https://crackstation.net/) benzeri ama çok daha basit bir örnek projedir. Sadece sha256sum değerleri ile çalışabilir.

&nbsp;

- ClickhouseDB
- [Hash Generator](src/hash-generator/)
- [Hash Station Frontend](src/hash-station/)
- [Hash Station API](src/hash-station-api/)
- Nginx (Reverse Proxy)
- [DB Cleaner](src/db-cleaner/)

&nbsp;

<div align="center">
     <img src=".img/fe1.png" width="80%" ></center>
</div>

&nbsp;

---

&nbsp;

### Projenin ayağa kaldırılması

#### Docker Compose

```sh
docker-compose up -d --build --force-recreate
```

&nbsp;

- [Hash Station](http://172.20.0.13)

&nbsp;

---

&nbsp;

### K8S / Minikube

```sh
minikube start
```

&nbsp;

#### Imajların oluşturulması:

```sh

# Sisteminizde taskfile yüklü ise sadece:

task

# Veya

docker build -q --push -t ttl.sh/hash-station/hash-generator   -f ./src/hash-generator/Dockerfile   ./src/hash-generator
docker build -q --push -t ttl.sh/hash-station/db-cleaner       -f ./src/db-cleaner/Dockerfile       ./src/db-cleaner
docker build -q --push -t ttl.sh/hash-station/hash-station-api -f ./src/hash-station-api/Dockerfile ./src/hash-station-api
docker build -q --push -t ttl.sh/hash-station/hash-station     -f ./src/hash-station/Dockerfile     ./src/hash-station
```

&nbsp;

#### Kubernetes'e deploy edilmesi:

```sh
kustomize build .kustomize/overlays/dev/ | kubectl apply -f -

```

```sh
kubectl get ing -n hashnet hashnet-ingress -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
```

&nbsp;

- [Hash Station](http://dev.hash-station.msenturk.net)

&nbsp;

---

&nbsp;

#### Kullanıcı oluşturulması:

```sh
# Kullanıcı oluşturmak için:
curl -s http://dev.hash-station.msenturk.net/api/register -u admin:pass -H "Content-Type: application/json" -d '{"username": "user", "password": "pass"}'

# Kullanıcı parolasını değiştirmek için:
curl -s -XPUT http://dev.hash-station.msenturk.net/api/register -u admin:pass -H "Content-Type: application/json" -d '{"username": "user", "password": "new_pass"}'

# Test için:
curl -s http://dev.hash-station.msenturk.net/api/login -H "Content-Type: application/json" -d '{"username": "user", "password": "new_pass"}'

```

&nbsp;

---

&nbsp;

### Yapılacaklar

- Health check
- Insert response / json

&nbsp;
