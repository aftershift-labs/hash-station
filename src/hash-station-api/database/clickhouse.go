package database

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/ClickHouse/clickhouse-go"
)

var DB *sql.DB

const (
	maxRetries = 10
	retryDelay = 10 * time.Second
)

func Init() {
	dbHost := os.Getenv("CLICKHOUSE_HOST")
	dbPort := os.Getenv("CLICKHOUSE_PORT")
	dbUser := os.Getenv("CLICKHOUSE_USER")
	dbPassword := os.Getenv("CLICKHOUSE_PASSWORD")
	dbName := os.Getenv("CLICKHOUSE_DB")

	connStr := fmt.Sprintf("tcp://%s:%s?username=%s&password=%s&database=%s",
		dbHost, dbPort, dbUser, dbPassword, dbName)

	var err error

	for i := 0; i < maxRetries; i++ {
		DB, err = sql.Open("clickhouse", connStr)
		if err == nil {
			err = DB.Ping()
			if err == nil {
				break
			}
		}

		log.Printf("Failed to connect to ClickHouse (attempt %d/%d): %v", i+1, maxRetries, err)
		time.Sleep(retryDelay)
	}

	if err != nil {
		log.Fatalf("Could not connect to ClickHouse after %d attempts: %v", maxRetries, err)
	}

	log.Println("Connected to ClickHouse")

	createUserTable()
	createHashTable()
}

func createUserTable() {
	query := `
	CREATE TABLE IF NOT EXISTS HASH.USER (
		username String,
		password String
	) ENGINE = MergeTree() ORDER BY username
	`
	_, err := DB.Exec(query)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("User table checked/created")
}

func createHashTable() {
	query := `
	CREATE TABLE IF NOT EXISTS HASH.HASH (
		text String,
		hash FixedString(64)
	) ENGINE = MergeTree() ORDER BY hash
	`
	_, err := DB.Exec(query)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Hash table checked/created")
}