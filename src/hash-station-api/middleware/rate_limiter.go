package middleware

import (
	"net/http"
	"strings"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

var UnauthorizedLimiter = rate.NewLimiter(1, 1)
var AuthorizedLimiter = rate.NewLimiter(rate.Every(5*time.Second), 1)
var limiterMap = make(map[string]*rate.Limiter)
var mu sync.Mutex

func getIP(r *http.Request) string {
	if forwarded := r.Header.Get("X-Forwarded-For"); forwarded != "" {
		ip := strings.Split(forwarded, ",")[0]
		return strings.TrimSpace(ip)
	}
	return r.RemoteAddr
}

func RateLimit(next http.HandlerFunc, limiter *rate.Limiter) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !limiter.Allow() {
			http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func RateLimitByIP(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ip := getIP(r)
		mu.Lock()
		if _, found := limiterMap[ip]; !found {
			limiterMap[ip] = rate.NewLimiter(1, 1)
			go func(ip string) {
				time.Sleep(10 * time.Minute)
				mu.Lock()
				delete(limiterMap, ip)
				mu.Unlock()
			}(ip)
		}
		limiter := limiterMap[ip]
		mu.Unlock()

		if !limiter.Allow() {
			http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)
			return
		}
		next.ServeHTTP(w, r)
	})
}
