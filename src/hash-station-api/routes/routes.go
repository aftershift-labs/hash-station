package routes

import (
	"github.com/gorilla/mux"
	"hash-station-api/handlers"
	"hash-station-api/middleware"
)

func InitializeRoutes() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/register", middleware.RateLimitByIP(handlers.RegisterHandler)).Methods("POST", "PUT")
	router.HandleFunc("/login", middleware.RateLimitByIP(handlers.LoginHandler)).Methods("POST")
	router.HandleFunc("/insert", middleware.IsAuthorized(middleware.RateLimit(handlers.InsertHandler, middleware.AuthorizedLimiter))).Methods("POST")
	router.HandleFunc("/search", handlers.SearchHandler).Methods("POST")
	router.HandleFunc("/count", handlers.CountHandler).Methods("GET")

	return router
}
