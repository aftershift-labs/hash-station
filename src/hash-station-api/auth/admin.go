package auth

import (
	"os"
)

var AdminUsername = os.Getenv("ADMIN_USERNAME")
var AdminPassword = os.Getenv("ADMIN_PASSWORD")

func CheckAdminCredentials(username, password string) bool {
	return username == AdminUsername && password == AdminPassword
}
