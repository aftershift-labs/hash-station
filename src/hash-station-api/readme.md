# Hash Station API

- Kullanıcı ve veritabanı arasında köprü görevi görür.

&nbsp;

### API uçları

|*Yol*|*Metod*|*Gerekli Parametreler*|*Yanıt*|*Açıklama*|
|---|:-:|:-:|:-:|---|
|/count|GET|-|application/json|Veritabanındaki toplam kayıt sayısını döndürür.|
|/insert|POST|text, hash|text/plain|Veritabanına yeni kayıt ekler.|
|/search|POST|hash|application/json|Veritabanında bir hash değerini arar.|
|/register|POST / PUT|Basic Auth, username, password|application/json|Hash ekleyebilecek yeni kullanıcı oluşturur/günceller.|
|/login|POST|username, password|application/json|Kullanıcı adı ve parola ile 5 dk geçerli JWT token alır.|

&nbsp;

### Örnekler

|*Yol*|*İstek / cURL*|
|---|---|
|/count|curl -s http://dev.hash-station.msenturk.net/api/count|
|/insert|curl -s http://dev.hash-station.msenturk.net/api/insert -H "Content-Type: application/json" -d '{"text": "sample", "hash": "af2bdbe1aa9b6ec1e2ade1d694f41fc71a831d0268e9891562113d8a62add1bf"}'|
|/insert|curl -s http://dev.hash-station.msenturk.net/api/insert -H "Content-Type: application/json" -d '{"text": "sample", "hash": "af2bdbe1aa9b6ec1e2ade1d694f41fc71a831d0268e9891562113d8a62add1bf"}'|
|/search|curl -s http://dev.hash-station.msenturk.net/api/search -d '{"hash": "af2bdbe1aa9b6ec1e2ade1d694f41fc71a831d0268e9891562113d8a62add1bf"}'|
|/search|curl -s http://dev.hash-station.msenturk.net/api/search -H "Content-Type: application/json" -d '{"hash": "af2bdbe1aa9b6ec1e2ade1d694f41fc71a831d0268e9891562113d8a62add1bf"}'|
|/register|curl -s http://dev.hash-station.msenturk.net/api/register -u admin:pass -H "Content-Type: application/json" -d '{"username": "user", "password": "pass"}'|
|/register|curl -s -XPUT http://dev.hash-station.msenturk.net/api/register -u admin:pass -H "Content-Type: application/json" -d '{"username": "user", "password": "new_pass"}'|
|/login|curl -s http://dev.hash-station.msenturk.net/api/login -H "Content-Type: application/json" -d '{"username": "user", "password": "new_pass"}'|

&nbsp;
