package handlers

import (
    "encoding/json"
    "net/http"
    "hash-station-api/database"
    "log"
)

func CountHandler(w http.ResponseWriter, r *http.Request) {
    var count int
    var totalBytesOnDisk uint64

    err := database.DB.QueryRow("SELECT COUNT(*) FROM HASH.HASH").Scan(&count)
    if err != nil {
        log.Printf("Error fetching hash count: %v", err)
        http.Error(w, "Internal Server Error", http.StatusInternalServerError)
        return
    }

    err = database.DB.QueryRow("SELECT sum(bytes_on_disk) AS total_bytes_on_disk FROM system.parts WHERE database = 'HASH' AND table = 'HASH'").Scan(&totalBytesOnDisk)
    if err != nil {
        log.Printf("Error fetching table size: %v", err)
        http.Error(w, "Internal Server Error", http.StatusInternalServerError)
        return
    }

    // Convert bytes to MB
    totalMBOnDisk := float64(totalBytesOnDisk) / (1024 * 1024)

    stats := map[string]interface{}{
        "count":            count,
        "totalMBOnDisk":    totalMBOnDisk,
    }

    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(stats)
}
