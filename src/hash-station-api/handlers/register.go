package handlers

import (
	"database/sql"
	"encoding/json"
	"hash-station-api/auth"
	"hash-station-api/database"
	"log"
	"net/http"
)

func RegisterHandler(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok || !auth.CheckAdminCredentials(username, password) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	var creds struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	err := json.NewDecoder(r.Body).Decode(&creds)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	hashedPassword, err := auth.HashPassword(creds.Password)
	if err != nil {
		log.Printf("Error hashing password: %v", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	var dbPassword string
	err = database.DB.QueryRow("SELECT password FROM HASH.USER WHERE username = ?", creds.Username).Scan(&dbPassword)

	if err != nil && err != sql.ErrNoRows {
		log.Printf("Error checking user existence: %v", err)
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	if err == sql.ErrNoRows {
		tx, err := database.DB.Begin()
		if err != nil {
			log.Printf("Error starting transaction: %v", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		stmt, err := tx.Prepare("INSERT INTO HASH.USER (username, password) VALUES (?, ?)")
		if err != nil {
			log.Printf("Error preparing statement: %v", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}
		defer stmt.Close()

		_, err = stmt.Exec(creds.Username, hashedPassword)
		if err != nil {
			tx.Rollback()
			log.Printf("Error executing statement: %v", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		if err := tx.Commit(); err != nil {
			tx.Rollback()
			log.Printf("Error committing transaction: %v", err)
			http.Error(w, "Internal Server Error", http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusCreated)
	} else {
		if r.Method == http.MethodPut {
			tx, err := database.DB.Begin()
			if err != nil {
				log.Printf("Error starting transaction: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			stmt, err := tx.Prepare("DELETE FROM HASH.USER WHERE username = ?")
			if err != nil {
				log.Printf("Error preparing statement: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}
			defer stmt.Close()

			_, err = stmt.Exec(creds.Username)
			if err != nil {
				tx.Rollback()
				log.Printf("Error executing statement: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			stmt, err = tx.Prepare("INSERT INTO HASH.USER (username, password) VALUES (?, ?)")
			if err != nil {
				log.Printf("Error preparing statement: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}
			defer stmt.Close()

			_, err = stmt.Exec(creds.Username, hashedPassword)
			if err != nil {
				tx.Rollback()
				log.Printf("Error executing statement: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			if err := tx.Commit(); err != nil {
				tx.Rollback()
				log.Printf("Error committing transaction: %v", err)
				http.Error(w, "Internal Server Error", http.StatusInternalServerError)
				return
			}

			w.WriteHeader(http.StatusOK)
		} else {
			http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		}
	}
}
