package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"hash-station-api/database"
	"log"
)

type InsertRequest struct {
	Text string `json:"text"`
	Hash string `json:"hash"`
}

func InsertHandler(w http.ResponseWriter, r *http.Request) {
	var req InsertRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	exists, err := recordExists(req.Hash)
	if err != nil {
		http.Error(w, "Failed to check for duplicate record", http.StatusInternalServerError)
		return
	}
	if exists {
		http.Error(w, "Duplicate record", http.StatusConflict)
		return
	}

	tx, err := database.DB.Begin()
	if err != nil {
		http.Error(w, "Failed to begin transaction", http.StatusInternalServerError)
		return
	}

	stmt, err := tx.Prepare("INSERT INTO HASH.HASH (text, hash) VALUES (?, ?)")
	if err != nil {
		http.Error(w, "Failed to prepare statement", http.StatusInternalServerError)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(req.Text, req.Hash)
	if err != nil {
		tx.Rollback()
		http.Error(w, "Failed to execute statement", http.StatusInternalServerError)
		return
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		http.Error(w, "Failed to commit transaction", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Record inserted successfully"))
	log.Printf("Inserted: Text=%s, Hash=%s", req.Text, req.Hash)
}

func recordExists(hash string) (bool, error) {
	query := fmt.Sprintf("SELECT COUNT(*) FROM HASH.HASH WHERE hash = '%s'", hash)
	var count int
	err := database.DB.QueryRow(query).Scan(&count)
	if err != nil {
		return false, err
	}
	return count > 0, nil
}
