package handlers

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"hash-station-api/database"
	"log"
)

type SearchRequest struct {
	Hash string `json:"hash"`
}

type SearchResponse struct {
	Text string `json:"text"`
}

func SearchHandler(w http.ResponseWriter, r *http.Request) {
	var req SearchRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, "Invalid request payload", http.StatusBadRequest)
		return
	}

	text, err := findTextByHash(req.Hash)
	if err != nil {
		http.Error(w, "Error occurred while searching for hash", http.StatusInternalServerError)
		return
	}
	if text == "" {
		http.Error(w, "No record found", http.StatusNotFound)
		return
	}

	response := SearchResponse{Text: text}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
	log.Printf("Found: Hash=%s, Text=%s", req.Hash, text)
}

func findTextByHash(hash string) (string, error) {
	query := "SELECT text FROM HASH.HASH WHERE hash = ?"
	var text string
	err := database.DB.QueryRow(query, hash).Scan(&text)
	if err != nil {
		if err == sql.ErrNoRows {
			return "", nil
		}
		return "", err
	}
	return text, nil
}
