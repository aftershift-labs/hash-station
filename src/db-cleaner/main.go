package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"regexp"
	"time"

	_ "github.com/ClickHouse/clickhouse-go"
)

const (
	maxRetries  = 10
	retryDelay  = 10 * time.Second
	fullRangeStart = "0000000000000000000000000000000000000000000000000000000000000000"
	fullRangeEnd   = "ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"
)

var validHashPattern = regexp.MustCompile(`^[a-f0-9]{64}$`)

func main() {
	dbHost := os.Getenv("CLICKHOUSE_HOST")
	dbPort := os.Getenv("CLICKHOUSE_PORT")
	dbUser := os.Getenv("CLICKHOUSE_USER")
	dbPassword := os.Getenv("CLICKHOUSE_PASSWORD")
	dbName := os.Getenv("CLICKHOUSE_DB")
	hashRangeStart := os.Getenv("HASH_RANGE_START")
	hashRangeEnd := os.Getenv("HASH_RANGE_END")

	// Çevre değişkenleri tanımlı değilse tüm aralığı tarar
	if hashRangeStart == "" {
		hashRangeStart = fullRangeStart
	}
	if hashRangeEnd == "" {
		hashRangeEnd = fullRangeEnd
	}

	connStr := fmt.Sprintf("tcp://%s:%s?username=%s&password=%s&database=%s", dbHost, dbPort, dbUser, dbPassword, dbName)

	var db *sql.DB
	var err error

	// Retry mekanizması
	for i := 0; i < maxRetries; i++ {
		db, err = sql.Open("clickhouse", connStr)
		if err == nil {
			err = db.Ping()
			if err == nil {
				break
			}
		}

		log.Printf("Failed to connect to ClickHouse (attempt %d/%d): %v", i+1, maxRetries, err)
		time.Sleep(retryDelay)
	}

	if err != nil {
		log.Fatalf("Could not connect to ClickHouse after %d attempts: %v", maxRetries, err)
	}
	defer db.Close()

	log.Printf("Starting to check hashes in range %s to %s", hashRangeStart, hashRangeEnd)
	checkAndCleanHashes(db, hashRangeStart, hashRangeEnd)
	log.Println("Finished checking hashes")
}

func checkAndCleanHashes(db *sql.DB, hashRangeStart, hashRangeEnd string) {
	query := "SELECT text, hash FROM HASH.HASH WHERE (hash >= ? AND hash <= ?) OR NOT match(hash, '^[a-f0-9]{64}$')"
	rows, err := db.Query(query, hashRangeStart, hashRangeEnd)
	if err != nil {
		log.Printf("Failed to query hashes: %v", err)
		return
	}
	defer rows.Close()

	var text, hash string
	for rows.Next() {
		if err := rows.Scan(&text, &hash); err != nil {
			log.Printf("Failed to scan row: %v", err)
			continue
		}

		if !isValidHash(hash) {
			log.Printf("Invalid hash format for text: %s. Deleting record.", text)
			if err := deleteRecord(db, hash); err != nil {
				log.Printf("Failed to delete record: %v", err)
			}
			continue
		}

		calculatedHash := calculateHash(text)
		if calculatedHash != hash {
			log.Printf("Hash mismatch for text: %s. Deleting record.", text)
			if err := deleteRecord(db, hash); err != nil {
				log.Printf("Failed to delete record: %v", err)
			}
		}
	}

	if err := rows.Err(); err != nil {
		log.Printf("Error occurred during row iteration: %v", err)
	}
}

func isValidHash(hash string) bool {
	return validHashPattern.MatchString(hash)
}

func calculateHash(text string) string {
	hash := sha256.Sum256([]byte(text))
	return hex.EncodeToString(hash[:])
}

func deleteRecord(db *sql.DB, hash string) error {
	_, err := db.Exec("ALTER TABLE HASH.HASH DELETE WHERE hash = ?", hash)
	if err == nil {
		log.Printf("Deleted record with hash: %s", hash)
	}
	return err
}
