# DB Cleaner

- Arayüz üzerinden hash ekleme yöntemi, hash'in istemci tarafında oluşturulup API'ye POST isteği ile iletilmesi olarak belirlenmiştir.
  
  - **Bu yöntem, istemci tarafında bir iş örneği teşkil edilmesi için tercih edilmiştir.**
  
  - **Canlı ortamda uygun bir kullanım senaryosu değildir.**

- Kullanıcı tarafından hatalı veri de eklenebilir.

- Bu hatalı verilerin periyodik olarak temizliği, db-cleaner ile yapılır.

&nbsp;

- HASH.HASH tablosundaki tüm satırlar tek tek kontrol edilerek hatalı hash değerleri içerenler silinir.

- DB üzerindeki tüm veri bölümlendirilerek(segmentation) birden fazla pod ile paralel işlem yürütülebilir.

&nbsp;
