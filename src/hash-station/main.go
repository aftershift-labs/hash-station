package main

import (
	"crypto/sha256"
	"encoding/hex"
	"syscall/js"
)

func generateHash(this js.Value, p []js.Value) interface{} {
	text := p[0].String()
	hash := sha256.Sum256([]byte(text))
	return hex.EncodeToString(hash[:])
}

func main() {
	js.Global().Set("generateHash", js.FuncOf(generateHash))

	select {}
}
