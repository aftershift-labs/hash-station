# Hash Station

- API ve kullanıcı arasında köprü görevi görür.

- Yeni kayıt ekleme işlemi:
  
  - Açık metnin hash değeri, webAssembly üzerinde çalışan, GO ile yazılmış bir fonksiyon ile istemci tarafında üretilir.

  - API üzerinden veritabanına eklenir.

  - **Bu yöntem, istemci tarafında bir iş örneği teşkil edilmesi için tercih edilmiştir.**

  - **Canlı ortamda uygun bir kullanım senaryosu değildir.**

&nbsp;

<div align="center">
     <img src="../../.img/fe1.png" width="80%" ></center>
</div>

&nbsp;

<div align="center">
     <img src="../../.img/fe3.png" width="80%" ></center>
</div>

&nbsp;

<div align="center">
     <img src="../../.img/fe2.png" width="80%" ></center>
</div>

&nbsp;


