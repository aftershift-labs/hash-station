package main

import (
	"bufio"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
	"unicode"

	_ "github.com/ClickHouse/clickhouse-go"
)

const (
	batchSize  = 100
	maxRetries = 10
	retryDelay = 10 * time.Second
)

func main() {
	fileFlag := flag.String("file", "", "Input file to process")
	randomFlag := flag.Int("random", 0, "Generate random entries")
	flag.Parse()

	if *fileFlag == "" && *randomFlag == 0 {
		log.Fatal("Usage: hash-generator --file <file> | --random <number_of_entries>")
	}

	dbHost := os.Getenv("CLICKHOUSE_HOST")
	dbPort := os.Getenv("CLICKHOUSE_PORT")
	dbUser := os.Getenv("CLICKHOUSE_USER")
	dbPassword := os.Getenv("CLICKHOUSE_PASSWORD")
	dbName := os.Getenv("CLICKHOUSE_DB")

	connStr := fmt.Sprintf("tcp://%s:%s?username=%s&password=%s", dbHost, dbPort, dbUser, dbPassword)

	var db *sql.DB
	var err error

	for i := 0; i < maxRetries; i++ {
		db, err = sql.Open("clickhouse", connStr)
		if err == nil {
			err = db.Ping()
			if err == nil {
				break
			}
		}

		log.Printf("Failed to connect to ClickHouse (attempt %d/%d): %v", i+1, maxRetries, err)
		time.Sleep(retryDelay)
	}

	if err != nil {
		log.Fatalf("Could not connect to ClickHouse after %d attempts: %v", maxRetries, err)
	}
	defer db.Close()

	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbName))
	if err != nil {
		log.Fatalf("Failed to create database: %v", err)
	}

	connStr = fmt.Sprintf("tcp://%s:%s?username=%s&password=%s&database=%s", dbHost, dbPort, dbUser, dbPassword, dbName)
	db, err = sql.Open("clickhouse", connStr)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}
	defer db.Close()

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS HASH.HASH (text String, hash FixedString(64)) ENGINE = MergeTree() ORDER BY hash")
	if err != nil {
		log.Fatalf("Failed to create table: %v", err)
	}

	if *randomFlag > 0 {
		fileName := "/generated.list"
		generateRandomEntries(fileName, *randomFlag)
		processFile(db, fileName)
		os.Remove(fileName)
	} else {
		processFile(db, *fileFlag)
	}
}

func generateRandomEntries(fileName string, numEntries int) {
	file, err := os.Create(fileName)
	if err != nil {
		log.Fatalf("Failed to create file: %v", err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < numEntries; i++ {
		text := generateRandomString(127)
		writer.WriteString(text + "\n")
	}
	writer.Flush()
}

func generateRandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[rand.Intn(len(charset))]
	}
	return string(b)
}

func processFile(db *sql.DB, fileName string) {
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	totalLines, err := countLines(file)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Total lines: %d\n", totalLines)

	file.Seek(0, 0)
	scanner := bufio.NewScanner(file)

	startTime := time.Now()
	lineNumber := 0
	batch := make([][2]string, 0, batchSize)
	committedLines := 0

	for scanner.Scan() {
		line := scanner.Text()
		lineNumber++

		if !isValidLine(line) {
			continue
		}

		hash := sha256.Sum256([]byte(line))
		hashString := hex.EncodeToString(hash[:])

		if recordExists(db, hashString) {
			log.Printf("Duplicate hash, skipping: %s", line)
			continue
		}

		batch = append(batch, [2]string{line, hashString})

		if len(batch) >= batchSize {
			if err := commitBatch(db, batch); err != nil {
				log.Fatalf("Failed to commit batch: %v", err)
			}
			committedLines += len(batch)
			batch = batch[:0]

			fmt.Printf("Processed %d/%d lines (%.2f%%)\n", committedLines, totalLines, float64(committedLines)/float64(totalLines)*100)
		}
	}

	if len(batch) > 0 {
		if err := commitBatch(db, batch); err != nil {
			log.Fatalf("Failed to commit batch: %v", err)
		}
		committedLines += len(batch)

		fmt.Printf("Processed %d/%d lines (%.2f%%)\n", committedLines, totalLines, float64(committedLines)/float64(totalLines)*100)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	duration := time.Since(startTime)
	fmt.Printf("Processing completed in %s\n", duration)
}

func countLines(file *os.File) (int, error) {
	scanner := bufio.NewScanner(file)
	lineCount := 0
	for scanner.Scan() {
		lineCount++
	}
	return lineCount, scanner.Err()
}

func isValidLine(line string) bool {
	if len(line) > 128 {
		return false
	}

	for _, r := range line {
		if !isAcceptableRune(r) {
			return false
		}
	}

	return true
}

func isAcceptableRune(r rune) bool {
	if unicode.IsLetter(r) || unicode.IsDigit(r) {
		return true
	}

	switch r {
	case ' ', ',', '.', ';', ':', '-', '_', '(', ')', '[', ']', '{', '}', '!', '?', '\'', '"', '/', '\\', '|', '@', '#', '$', '%', '^', '&', '*', '+', '=', '<', '>', '~', '`':
		return true
	}

	return false
}

func recordExists(db *sql.DB, hash string) bool {
	var count int
	err := db.QueryRow("SELECT COUNT(*) FROM HASH.HASH WHERE hash = ?", hash).Scan(&count)
	if err != nil {
		log.Printf("Failed to check record existence: %v", err)
		return false
	}
	return count > 0
}

func commitBatch(db *sql.DB, batch [][2]string) error {
	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO HASH.HASH (text, hash) VALUES (?, ?)")
	if err != nil {
		return err
	}
	defer stmt.Close()

	for _, record := range batch {
		_, err := stmt.Exec(record[0], record[1])
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("failed to execute statement: %w", err)
		}
	}

	if err := tx.Commit(); err != nil {
		tx.Rollback()
		return fmt.Errorf("failed to commit transaction: %w", err)
	}

	return nil
}
