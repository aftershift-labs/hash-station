# Hash Generator

- Veritabanı üzerinde toplu veri üretimi için kullanılır.

- **Rastgele veri üretimi:**

  - **<code>/hash-generator --random 10</code>:** 10 adet rastgele metin üretip hash değerlerini veritabanına ekler.

  - İstenen sayıda rastgele verinin tamamı bir dosyaya yazılır.

  - Ardından --file parametresindeki senaryo işletilir.

- **Bir dosyadan veri üretimi:**

  - **<code>/hash-generator --file /hash.list</code>:** /hash.list dosyasındaki her satırın hash değeri üretilerek veritabanına eklenir.

  - batchSize=100 olarak belirlenmiştir.

    - Her 100 satırda bir veritabanına commit gerçekleşir.

&nbsp;

---

&nbsp;

- Servis ayağa kalktığında gerekli veritabanı ve tablonun oluşturulmasını sağlar.

  - Tablo oluşturma sorgusu: <code>CREATE TABLE IF NOT EXISTS HASH.HASH (text String, hash FixedString(64)) ENGINE = MergeTree() ORDER BY hash</code>

  - Tablo, text ve hash sütunlarından oluşur ve hash sütununa göre sıralanır.